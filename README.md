This module hooks up Drupal to the Lastpass SAML. Install this plugin and then visit the configuration area to setup the lastpass SAML settings then you can visit /lastpass_login and it will log you into your account. New users are automatically created and assigned the role(s) you set in the settings area.

If you wish to use this for the general public you would need to redirect /user to /lastpass_login.
